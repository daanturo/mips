.data
arr:		.float 7.0 10.25 7.75 -8.25 15.25 10.0 5.0 13.5 -13.75 -14.5 1.25 -11.75 14.5 9.5 -5.5 -5.75 -6.25 -14.25 1.75 12.25
endArr:	.float
minOut:	.asciiz "\nMin: "
maxOut:	.asciiz "\nMax: "
.text
main:
	la $a0, arr
	la $a1, endArr
 	#min = arr[0]
	lwc1 $f0, 0($a0)
	#max = arr[0]
	lwc1 $f2, 0($a0)
	while:
		addi $a0, $a0, 4
		# stop if current address reaches the end
		beq $a0, $a1, endwhile
		# load arr[i]
		lwc1 $f1, 0($a0)
		# code = ( float < min )
		c.lt.s $f1, $f0
		# skip if float>=min
		bc1f min_not_changed
		mov.s $f0, $f1
		min_not_changed:
		# code = ( max < float )
		c.lt.s $f2, $f1
		# skip if max >= float
		bc1f max_not_changed
		mov.s $f2, $f1
		max_not_changed:
		j while
	endwhile:
	
	li $v0, 4
	la $a0, minOut
	syscall
	
	li $v0, 2
	mov.s $f12, $f0
	syscall
	
	li $v0, 4
	la $a0, maxOut
	syscall
	
	li $v0, 2
	mov.s $f12, $f2
	syscall
exit:
	li $v0, 10
	syscall
