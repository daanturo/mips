#Chuong trinh: ten chuong trinh
#Data segment
	.data

#Cac dinh nghia bien
buffer: .space 10
a:	.word
b:	.word
c:	.word
ktmt19:  .asciiz "\nKien Truc May Tinh 2019\n"
instring: .asciiz
Nhap_so:.asciiz "Nhap so nguyen: "
ina:	.asciiz "nhap a: "
inb:	.asciiz "nhap b: "
inc:	.asciiz "nhap c: "
req:	.asciiz "nhap chuoi: "
endl:	.asciiz "\n"
#Code segment

	.text
	.globl	main
main:
	li	$v0, 4
	la	$a0, ina
	syscall
	
	li	$v0, 5
	syscall
	add	$s1,$zero,$v0
	#
	li	$v0, 4
	la	$a0, inb
	syscall
	
	li	$v0, 5
	syscall
	add	$s2,$zero,$v0
	#
	li	$v0, 4
	la	$a0, inc
	syscall
	
	li	$v0, 5
	syscall
	add	$s3,$zero,$v0
	#
	li	$v0, 1
	sub	$t0, $s1, $s2
	add	$a0, $t0, $s3
	syscall
	
	li	$v0, 4
	la	$a0, ktmt19
	syscall
	
	li	$v0, 4
	la	$a0, req
	syscall	
	
	li $v0,8		#call for input string service
	la $a0, buffer
#	li $a1, 10
	
	move $t0, $a0
	syscall
	
	la $a0, buffer
	move $a0, $t0
	li $v0, 4
	syscall
	
	j	Ket_thuc
#Xu ly

#xuat ket qua
#Xuat:	lw	$a0,so+4
#	addiu 	$v0,$zero,1
#	syscall
	
Ket_thuc:
	#addiu $v0,$zero,10
	li $v0, 10
	syscall
	
