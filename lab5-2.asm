.data
pi:		.float 3.14159
input:	.asciiz "\nRadius: "
perimeter:	.asciiz "\nPerimeter: "
area:		.asciiz "\nArea: "
.text
main:
	li $v0, 4
	la $a0, input
	syscall
	
	li $v0, 6
	syscall
	# f0 = radius
	
	mtc1 $0, $f4
	# if r <= 0
	c.le.s $f0, $f4
	bc1t main
	
	ldc1 $f4, pi
	# f2 = r*2
	add.s	$f2, $f0, $f0
	# f2 = f2 * pi
	mul.s $f2, $f2, $f4
	
	li $v0, 4
	la $a0, perimeter
	syscall
	
	mov.s $f12, $f2
	li $v0, 2
	syscall
	
	# f2 = r*r
	mul.s	$f2, $f0, $f0
	# f2 = f2 * pi
	mul.s $f2, $f2, $f4
	
	li $v0, 4
	la $a0, area
	syscall
	
	mov.s $f12, $f2
	li $v0, 2
	syscall
exit:
	li $v0, 10
	syscall
	