.data
sDientich: .asciiz "Dien tich tu giac: "
sxA: .asciiz "x A : "
syA: .asciiz "y A : "
sxB: .asciiz "x B : "
syB: .asciiz "y B : "
sxC: .asciiz "x C : "
syC: .asciiz "y C : "
sxD: .asciiz "x D : "
syD: .asciiz "y D : "
.align 2
half: .float 0.5
.text
.globl main
main:
  li $v0, 4
  la $a0, sxA
  syscall
  li $v0, 6
  syscall
  mov.s $f1, $f0
  
  li $v0, 4
  la $a0, syA
  syscall
  li $v0, 6
  syscall
  mov.s $f2, $f0
  
  li $v0, 4
  la $a0, sxB
  syscall
  li $v0, 6
  syscall
  mov.s $f3, $f0
  
  li $v0, 4
  la $a0, syB
  syscall
  li $v0, 6
  syscall
  mov.s $f4, $f0
  
  li $v0, 4
  la $a0, sxC
  syscall
  li $v0, 6
  syscall
  mov.s $f5, $f0
  
  li $v0, 4
  la $a0, syC
  syscall
  li $v0, 6
  syscall
  mov.s $f6, $f0
  
  li $v0, 4
  la $a0, sxD
  syscall
  li $v0, 6
  syscall
  mov.s $f7, $f0
  
  li $v0, 4
  la $a0, syD
  syscall
  li $v0, 6
  syscall
  mov.s $f8, $f0
  
  
  mtc1 $0, $f12
  
  mul.s $f9, $f1, $f4
  mul.s $f10, $f3, $f2
  sub.s $f11, $f9, $f10
  add.s $f12, $f12, $f11
  
  mul.s $f9, $f3, $f6
  mul.s $f10, $f5, $f4
  sub.s $f11, $f9, $f10
  add.s $f12, $f12, $f11
  
  mul.s $f9, $f5, $f8
  mul.s $f10, $f7, $f6
  sub.s $f11, $f9, $f10
  add.s $f12, $f12, $f11
  
  mul.s $f9, $f7, $f2
  mul.s $f10, $f1, $f8
  sub.s $f11, $f9, $f10
  add.s $f12, $f12, $f11
  
  abs.s $f12, $f12
  lwc1 $f0, half
  mul.s $f12, $f12, $f0
  
  li $v0, 2
  syscall
