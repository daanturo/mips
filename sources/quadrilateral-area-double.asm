# L07 - 1810887 - Lê Trung Đan
.data
half_d: .double 0.5
sInN: .asciiz "Nhap so dinh cua da giac: "
sOrder: .asciiz "Chu y nhap dung thu tu cac diem.\n"
sInCoor: .asciiz "Nhap toa do cua diem thu: "
sX: .asciiz "\nHoanh do x: "
sY: .asciiz "Tung do y: "
sRes: .asciiz "\nKet qua: "
.text
.globl main

# reference: en.wikipedia.org/wiki/Polygon#Area
# Area = | Sum of ( x[i] * y[i+1] - x[i+1] * y[i] ) | / 2
# i runs from 0 to n-1 inclusively, with x[n] = x[0] and y[n] = y[0]
main:
#   print "Chu y nhap dung thu tu cac diem."
  li $v0, 4
  la $a0, sOrder
  syscall
#   s0 := number of points
  li $s0, 4
#   Uncomment lines inside "##" to calculate area of a polygon with arbitrary number of vertexes
##
#   li $v0, 4
#   la $a0, sInN
#   syscall
#   li $v0, 5
#   syscall
#   addi $s0, $v0, 0
##
#   only s0 * 2 double-precision floating-point numbers are needed to be stored, which is s0 * 2 * 8 bytes
#   but the first point's coordinates is appended at the end as a virtual point to ease calculation (x[n] = x[0] and y[n] = y[0])
#   s1 := space to allocate = s0 * 16 + 16, but double must be stored on an address that is divisible by 8
  sll $s1, $s0, 4
  addi $s1, $s1, 16
#   t0 = sp % 8
  andi $t0, $sp, 0x7
#   s2 := the virtual point's address
  addi $s2, $sp, -16
#   skip adding 4 to s1 and relocate s2 if the stack address is already divisible by 8 to store double-precision floats
  beq $t0, $0, made_sure_stack_is_aligned_on_double_boundary
  addi $s1, $s1, 4
  addi $s2, $s2, -4
  made_sure_stack_is_aligned_on_double_boundary:
#   allocate memory on the stack
  sub $sp, $sp, $s1
#   saved elements on stack:
#   x1, y1, x2, y2,...
#   t0 := iterator of elements' addresses
  addi $t0, $sp, 0
#   t1 := current point's NO. (one-based), for displaying only
  addi $t1, $0, 1
  while_reading:
#     stop if current address reach the virtual point's
    beq $t0, $s2, end_reading
#     print "Nhap toa do cua diem thu: "
    li $v0, 4
    la $a0, sInCoor
    syscall
#     print current point's NO.
    li $v0, 1
    addi $a0, $t1, 0
    syscall
#     print "Hoanh do x: "
    li $v0, 4
    la $a0, sX
    syscall
#     store current point's x-coordinate
    li $v0, 7
    syscall
    sdc1 $f0, 0($t0)
#     print "Tung do x: "
    li $v0, 4
    la $a0, sY
    syscall
#     store current point's y-coordinate
    li $v0, 7
    syscall
    sdc1 $f0, 8($t0)
#     process to the next point
    addi $t0, $t0, 16
    addi $t1, $t1, 1
    j while_reading
  end_reading:
#   save the virtual point:
#   x[n] = x[0] and y[n] = y[0]
  ldc1 $f0, 0($sp)
  sdc1 $f0, 0($s2)
  ldc1 $f0, 8($sp)
  sdc1 $f0, 8($s2)
#   a0: := iterator of the elements's addresses
  addi $a0, $sp, 0
#   let double f30 = 0.0
  mtc1 $0, $f30
  mtc1 $0, $f31
  while_calculating:
#     f0 = x[i] * y[i+1] - x[i+1] * y[i]
#     each temporary f0 is accumulated to f30, then f30 is divided by 2 and return it's absolute value
    jal calcSubDet
#     f30 += f0
    add.d $f30, $f30, $f0
#     process to the next point
    addi $a0, $a0, 16
#     stop if a0 reaches the virtual point's address
    beq $a0, $s2, stop_calculating
    j while_calculating
  stop_calculating:
#   print "Result: "
  li $v0, 4
  la $a0, sRes
  syscall
#   f12 := final result to print
#   f0 = 0.5
  ldc1 $f0, half_d
#   f12 = f31 * 0.5
  mul.d $f12, $f0, $f30
#   f12 = |f12|
  abs.d $f12, $f12
#   print it
  li $v0, 3
  syscall
#   restore the stack
  add $sp, $sp, $s1
  j exit
  
  
calcSubDet:
#   let f2 = x[i], f4 = y[i], f6 = x[i+1], f8 = y[i+1]
  ldc1 $f2, 0($a0)
  ldc1 $f4, 8($a0)
  ldc1 $f6, 16($a0)
  ldc1 $f8, 24($a0)
#   recall: Area = | Sum of ( x[i] * y[i+1] - x[i+1] * y[i] ) | / 2
#   f10 = x[i] * y[i+1]
  mul.d $f10, $f2, $f8
#   f12 = x[i+1] * y[i]
  mul.d $f12, $f6, $f4
#   f0 = x[i] * y[i+1] - x[i+1] * y[i]
  sub.d $f0, $f10, $f12
  jr $ra
  
#   exit the program
exit:
  li $v0, 10
  syscall
  j exit
