.data
arr: .word -55, 26,  -20, -60, -24, 27,  -13, -16, -40, -18, 5,   34, 11, 5,   61,  55,  -21, 47,  43,  40,  28,  -61, 50,  -38, 0,  5, 29,  -2,  35,  -27, -13, -38, -62, 32,  -5,  -30, 56,  20, -56, -16, -32, 61,  -58, 17,  26,  -42, 1,   -14, 20,  -4
endArr: .word
sUnsorted: .asciiz "\nUnsorted: "
sSorted: .asciiz "\nSorted: "
sEndl: .asciiz "\n"
sSpace: .asciiz " "
sSubUnsorted: .asciiz "\nSub array unsorted: "
sSubSorted: .asciiz "\nSub array sorted: "
sShowIndex: .asciiz "\nElements from position "
sTo: .asciiz " to "
.text
.globl main

main:
  addi $s3, $0, 0
  addi $s5, $0, 0
# print "Unsorted :"
  li $v0, 4
  la $a0, sUnsorted
  syscall
#   load the address of the array to a1 and its end (not inclusive) to a1
  la $a0, arr
  la $a1, endArr
#   print the array

  addi $s3, $s3, 1
  jal print
#   perform merge sort

  addi $s3, $s3, 1
  jal merge_sort
  
  addi $s3, $s3, 1
  j exit

#   merge sort algolrithm
merge_sort:
  addi $sp, $sp, -20
#   a0: start of array
#   a1: end of array (not inclusive)
#   a2: midpoint of the array
#   s0 = range between start and end
  sw $a0, 0($sp)
  sw $a1, 4($sp)
  sw $a2, 8($sp)
  sw $s0, 12($sp)
  sw $ra, 16($sp)
  sub $s0, $a1, $a0
#   check if there is less than 2 elements
  slti $t1, $s0, 8
#   terminate the function if ( a1 - a0 < 8 )

  addi $s3, $s3, 1
  bne $t1, $0, end_sort
#   t2 := t0 / 2 : midpoint's offset
#   t2 = t0 / 4 (word length) / 2 (to round down)
  srl $t2, $s0, 3
#   t2 = t2 * 4 (restore word length)
  sll $t2, $t2, 2
  add $a2, $a0, $t2
#   sort from the beginning to the middle
  add $a1, $0, $a2
  
  addi $s3, $s3, 1
  jal merge_sort
#   sort from the middle to the end
  add $a0, $a2, $0
  
  addi $s5, $s5, 1
  lw $a1, 4($sp)
  
  addi $s3, $s3, 1
  jal merge_sort
  
#   print the range of positions of current elements

  addi $s5, $s5, 1
  lw $a0, 0($sp)
  
  addi $s3, $s3, 1
  jal printIndex
#   print "Sub array unsorted:"
  li $v0, 4
  la $a0, sSubUnsorted
  syscall
#   restore the start of the array

  addi $s5, $s5, 1
  lw $a0, 0($sp)
#   print out the array

  addi $s3, $s3, 1
  jal print
#   allocate space for the auxiliary array on the stack
#   this array is used to temporarily store sorted elements
  sub $sp, $sp, $s0
#   s1: iterator for the auxiliary array
  addi $s1, $sp, -4
#   t1: iterator from the beginning of the array
  add $t1, $a0, $0
#   t2: iterator from the middle of the array
  add $t2, $a2, $0
  while_merging:
#   process to the next address on auxiliary array
    addi $s1, $s1, 4
#     t3 := mid - t1
    sub $t3, $a2, $t1
#     t4 := end - t2
    sub $t4, $a1, $t2
#     t0 := mid  - t1 + end - t2
    add $t0, $t3, $t4
#     stop merging if both subarrays are merged ( t3 + t4 == 0 )

    addi $s3, $s3, 1
    beq $t0, $0, end_merging
    
    addi $s5, $s5, 1
    lw $t5, 0($t1)
    
    addi $s5, $s5, 1
    lw $t6, 0($t2)
#     if the beginning to middle elements are merged, merge the right subarray

    addi $s3, $s3, 1
    beq $t1, $a2, add_from_t2
#     if the middle to ending elements are merged, merge the left subarray
    
    addi $s3, $s3, 1
    beq $t2, $a1, add_from_t1
#     t7 := current right < current left
    slt $t7, $t6, $t5
#     add current right if it's less than current left to preserve stability
    
    addi $s3, $s3, 1
    bne $t7, $0, add_from_t2
#     else add from the left
    add_from_t1:
      sw $t5, 0($s1)
      addi $t1, $t1, 4
      
      addi $s3, $s3, 1
      j while_merging
    add_from_t2:
      sw $t6, 0($s1)
      addi $t2, $t2, 4
      
      addi $s3, $s3, 1
      j while_merging
  end_merging:
#   copy data from the auxiliary array back to original array
#   start from the beginning of the auxiliary array
#   s1 : iterator of the auxiliary array
#   t1 : iterator of the original array
  add $s1, $sp, $0
  add $t1, $a0, $0
  while_copying:
#   stop copying if t1 reaches the end

    addi $s3, $s3, 1
    beq $t1, $a1, end_copying
#   copy value from the auxiliary array and store it to the oringinal array in the corresponding position
    
    addi $s5, $s5, 1
    lw $t0, 0($s1)
    sw $t0, 0($t1)
    addi $s1, $s1, 4
    addi $t1, $t1, 4
    
    addi $s3, $s3, 1
    j while_copying
  end_copying:
#   restore the stack, free the auxiliary array
  add $sp, $sp, $s0
  li $v0, 4
  la $a0, sSubSorted
  syscall
  
  addi $s5, $s5, 1
  lw $a0, 0($sp)
  
  addi $s3, $s3, 1
  jal print
  end_sort:
#   restore the stack
  addi $s5, $s5, 1
  lw $a0, 0($sp)
  addi $s5, $s5, 1
  lw $a1, 4($sp)
  addi $s5, $s5, 1
  lw $a2, 8($sp)
  addi $s5, $s5, 1
  lw $s0, 12($sp)
  addi $s5, $s5, 1
  lw $ra, 16($sp)
  addi $sp, $sp, 20
  jr $ra
  
#   print the array elements to the console
print:
#   a0: start of the array to print
#   a1: end of the array to print (not inclusive)
  addi $sp , $sp, -8
  sw $a0, 0($sp)
  sw $s0, 4($sp)
#   s0: iterator of the array to print
  add $s0, $a0, $0
  while_print:
#     stop if s0 reaches the end the array

    addi $s3, $s3, 1
    beq $s0, $a1, end_print
#     print the current element
    li $v0, 1
    
    addi $s5, $s5, 1
    lw $a0, 0($s0)
    syscall
#     print " "
    li $v0, 4
    la $a0, sSpace
    syscall
#     process to the next one
    addi $s0, $s0, 4
    
    addi $s3, $s3, 1
    j while_print
  end_print:
    li $v0, 4
    la $a0, sEndl
    syscall
#   restore the stack

  addi $s5, $s5, 1
  lw $a0, 0($sp)

  addi $s5, $s5, 1
  lw $s0, 4($sp)
  
  addi $sp, $sp, 8
  jr $ra
  
#   print the start and end index of the subarray counting from the start
printIndex:
#   a0 := start of the subarray
#   a1 := end of the subarray (not inclusive)
  addi $sp, $sp, -4
  sw $a0, 0($sp)
  add $t0, $a0, $0
#   print "Elements from position "
  li $v0, 4
  la $a0, sShowIndex
  syscall
#   t1 := start of the original array
  la $t1, arr
#   t2 := offset of the start of subarray from the original in term of bytes
  sub $t2, $t0, $t1
  srl $t2, $t2, 2
#   print t2
  li $v0, 1
  add $a0, $t2, $0
  syscall
#   print " to "
  li $v0, 4
  la $a0, sTo
  syscall
#   t2 := offset of the end of subarray from the original in term of bytes
  sub $t2, $a1, $t1
  srl $t2, $t2, 2
#   the end is not inclusive, so -1
  addi $t2, $t2, -1
#   print t2
  li $v0, 1
  add $a0, $t2, $0
  syscall
#   restore a0 from the stack

  addi $s5, $s5, 1
  lw $a0, 0($sp)
  addi $sp, $sp, 4
  jr $ra
  
#   exit the program
exit:
  li $v0, 10
  syscall
