#include <stdio.h>
#include <stdlib.h>

void print(int *start, int *end);
void merger_sort(int *start, int *end);

int main() {
  int arr[] = {-55, 26,  -20, -60, -24, 27,  -13, -16, -40, -18, 5,   34, 11,
               5,   61,  55,  -21, 47,  43,  40,  28,  -61, 50,  -38, 0,  5,
               29,  -2,  35,  -27, -13, -38, -62, 32,  -5,  -30, 56,  20, -56,
               -16, -32, 61,  -58, 17,  26,  -42, 1,   -14, 20,  -4};
  // end is not inclusive
  int size = 50;
  print(arr, arr + size);
  merger_sort(arr, arr + size);
  puts("");
  print(arr, arr + size);
  return 0;
}

void print(int *start, int *end) {
  for (int *i = start; i < end - 1; i++)
    printf("%d, ", *i);
  printf("%d\n", *(end - 1));
}

void merger_sort(int *start, int *end) {
  if (end - start < 2)
    return;
  int *mid = start + (end - start) / 2;
  merger_sort(start, mid);
  merger_sort(mid, end);
  int *tempArr = malloc((end - start) * sizeof(int));
  int *runner = tempArr;
  int *p1 = start;
  int *p2 = mid;
  //   while (!(p1 == mid && p2 == end)) {
  while (p1 != mid || p2 != end) {
    if (p1 == mid || (p2 != end && *p2 < *p1)) {
      *runner = *p2;
      p2 += 1;
    } else /*if (p2 == end || *p1 <= *p2)*/ {
      *runner = *p1;
      p1 += 1;
    }
    runner += 1;
  }
  for (int i = 0; i < end - start; i++) {
    start[i] = tempArr[i];
  }
  free(tempArr);
}
