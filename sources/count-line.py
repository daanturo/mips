fileIn = open ('merge-sort.asm', 'r') 
fileOut = open ('count-lw-merge-sort.asm', 'w')

list = []
count = []
for line in fileIn:
    lineL = line.split()
    if len(lineL) > 0:
        if ( lineL[0] in list ):
            count[ list.index(lineL[0]) ] += 1
        else:
            list.append( lineL[0] )
            count.append(1)
    if "lw" in line:
        fileOut.write("addi $s5, $s5, 1\n")
    if "syscall" in line:
        fileOut.write("#"+line)
    else:
        fileOut.write(line)
    if "main:" in line:
        fileOut.write("addi $s5, $0, 0\n")
fileIn.close()
fileOut.close()

for i in range ( len(list) ):
    name = list[i].upper()
    if not ( ':' in name or ',' in name or '.' in name or '#' in name):
        print(f"\item {name}: \t {count[i]}")
                
arr = "17704 17596 17706 17676 17709 17685 17690 17680 17702 17702 17681 17696"
arr = arr.split()
average = 0
for i in range (len(arr)):
    average += int( arr[i] )

average /= len(arr)
total = average
clkrate = 3.4 * 10 ** 9

print(f"Average instructions ran: {total}")
print(f"CPU time single-cycle\n {total / clkrate * 10 ** 6} * 10 ^ -6 s")