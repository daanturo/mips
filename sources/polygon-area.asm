.data
half_f: .float 0.5
sInN: .asciiz "Nhap so dinh cua da giac, chu y dung thu tu: "
sOrder: .asciiz "Chu y nhap dung thu tu cac diem.\n"
sInCoor: .asciiz "Nhap toa do cua diem thu: "
sX: .asciiz "\nHoanh do x: "
sY: .asciiz "Tung do y: "
sRes: .asciiz "\nKet qua: "
.text
.globl main

# reference: en.wikipedia.org/wiki/Polygon#Area
# Area = | Sum of ( x[i] * y[i+1] - x[i+1] * y[i] ) | / 2
# i runs from 0 to n-1, with x[n] = x[0] and y[n] = y[0]
main:
#   print "Nhap so dinh cua da giac: "
  li $v0, 4
  la $a0, sInN
  syscall
#   get number of points from user input
  li $v0, 5
  syscall
#   s0 := number of points
  add $s0, $v0, $0
#   print "Chu y nhap dung thu tu cac diem."
  li $v0, 4
  la $a0, sOrder
  syscall
#   only s0 * 2 float numbers is need to store, which is s0 * 2 * 4 bytes
#   but the first point's coordinates is appended at the end as a virtual point to ease calculation (x[n] = x[0] and y[n] = y[0])
#   s1 := space to allocate = s0 * 8 + 8
  sll $s1, $s0, 3
  addi $s1, $s1, 8
#   s2 := the virtual point's address
  add $s2, $sp, $0
  addi $s2, $s2, -8
#   allocate memory on the stack
  sub $sp, $sp, $s1
#   saved floats format:
#   x1, y1, x2, y2,...
#   t0 := iterator of elements' addresses
  add $t0, $sp, $0
#   t1 := current point's NO.(one-based), for displaying only
  addi $t1, $0, 1
  while_reading:
#     stop if current address reach the virtual point's
    beq $t0, $s2, end_reading
#     print "Nhap toa do cua diem thu: "
    li $v0, 4
    la $a0, sInCoor
    syscall
#     print current point's NO.
    li $v0, 1
    addi $a0, $t1, 0
    syscall
#     print "Hoanh do x: "
    li $v0, 4
    la $a0, sX
    syscall
#     store current point's x-coordinate
    li $v0, 6
    syscall
    swc1 $f0, 0($t0)
#     print "Tung do x: "
    li $v0, 4
    la $a0, sY
    syscall
#     store current point's y-coordinate
    li $v0, 6
    syscall
    swc1 $f0, 4($t0)
#     process to the next point
    addi $t0, $t0, 8
    addi $t1, $t1, 1
    j while_reading
  end_reading:
#   save the virtual point:
#   (x[n] = x[0] and y[n] = y[0])
  lwc1 $f0, 0($sp)
  swc1 $f0, 0($s2)
  lwc1 $f0, 4($sp)
  swc1 $f0, 4($s2)
#   a0: := iterator of the elements's addresses
  add $a0, $sp, $0
#   let f31 = 0.0
  mtc1 $0, $f31
  while_calculating:
#     f0 = x[i] * y[i+1] - x[i+1] * y[i]
    jal calcSubDet
#     f31 += f0
    add.s $f31, $f31, $f0
#     process to the next point
    addi $a0, $a0, 8
#     stop if a0 reaches the virtual point's address
    beq $a0, $s2, stop_calculating
    j while_calculating
  stop_calculating:
#   print "Result: "
  li $v0, 4
  la $a0, sRes
  syscall
#   f12 := final result to print
#   f0 = 0.5
  lwc1 $f0, half_f
#   f12 = f31 * 0.5
  mul.s $f12, $f0, $f31
#   f12 = |f12|
  abs.s $f12, $f12
#   print it
  li $v0, 2
  syscall
#   restore the stack
  add $sp, $sp, $s1
  j exit
  
  
calcSubDet:
#   let f1 = x[i], f2 = y[i], f3 = x[i+1], f4 = y[i+1]
  lwc1 $f1, 0($a0)
  lwc1 $f2, 4($a0)
  lwc1 $f3, 8($a0)
  lwc1 $f4, 12($a0)
#   recall: Area = | Sum of ( x[i] * y[i+1] - x[i+1] * y[i] ) | / 2
#   f5 = x[i] * y[i+1]
  mul.s $f5, $f1, $f4
#   f6 = x[i+1] * y[i]
  mul.s $f6, $f3, $f2
#   f0 = x[i] * y[i+1] - x[i+1] * y[i]
  sub.s $f0, $f5, $f6
  jr $ra
  
#   exit the program
exit:
  li $v0, 10
  syscall
  j exit
