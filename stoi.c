int strlen(char* str){
  int res = 0;
  while (str[res]!='\0'){
    res++;
  }
  return res;
}

int tenpower(int n){
  int res = 1;
  for (int i = 0 ;i<n;i++){
    res = res *10;
  }
  return res;
}

int stoi (char* str) {
  int temp = 0;
  int i,j, n=strlen(str);
  for (i=0;i<n;i++){
    j=str[i]-0x30;
    temp=temp+j*tenpower(n-i-1);
  }
  return temp;
}
