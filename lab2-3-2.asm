.data
	array: .word 1,2,3,4,-10,6,20,8,9
	size: .word
.text

main:
	la $a0, array
	lw $a1, size
	jal max
	add $s1, $v0, $0 #max
	jal min
	add $s2, $v0, $0 #min
	sub $a0, $s1, $s2
	li $v0, 1
	syscall
	j exit
min:
	addi $t0, $0, 1 #i=1
	addi $sp, $sp, -4
	sw $a0, 0($sp)
	lw $v0, 0($a0) #res = arr[0]
	addi $a0, $a0, 4
	whileMin:
		beq $t0, $a1, endMin
		lw $t1, 0($a0) #arr[i]
		blt $v0, $t1, ignoreMin # if arr[i] > res, ignore
		add $v0, $t1, $0 # res = arr[i]
		ignoreMin:
		addi $a0, $a0, 4
		addi $t0, $t0, 1
		j whileMin
	endMin:
	lw $a0, 0($sp)
	addi $sp, $sp, 4
	jr $ra
max:
	addi $t0, $0, 1 #i=1
	addi $sp, $sp, -4
	sw $a0, 0($sp)
	lw $v0, 0($a0) #res = arr[0]
	addi $a0, $a0, 4
	whileMax:
		beq $t0, $a1, endMax
		lw $t1, 0($a0) #arr[i]
		blt $t1, $v0, ignoreMax # if arr[i] < res, ignore
		add $v0, $t1, $0 # res = arr[i]
		ignoreMax:
		addi $a0, $a0, 4
		addi $t0, $t0, 1
		j whileMax
	endMax:
	lw $a0, 0($sp)
	addi $sp, $sp, 4
	jr $ra
exit:
	li $v0, 10
	syscall
