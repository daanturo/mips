.data

str: .ascii "1010"

.text

main:
	la $a0 str
	jal stoi
	add $s1, $v0, $0
	
	li $v0, 1
	add $a0, $s1, $0
	syscall
	
	j exit

stoi:	
	add $s2 , $0 , $0#return value
	add $s3 , $0 , $0#i
	add $s1 , $0 , $0#j
	addi $sp, $sp, -4 #store return address
	sw $ra, 0($sp)
	#la $a0, str
	jal strlen
	add $s0, $0, $v0 #string length
	lw $ra, 0($sp) #restore return address
	add $sp, $sp, 4 #restore stack
	whileStoi:
	beq $s3, $s0, stopStoi
	sll $t3, $s3, 2 #get the offset
	add $t4, $t3, $a0 #get the address of character
	#subi $t2, 0($t4), 0x30 #str[i] - 0x30
	addi $t5, $0, 0x30
	lb $t7 ,0($t4)
	sub $s3, $t7, $t5
	addi $sp, $sp, -4 #save the return address
	sw $ra, 0($sp)
	sub $a0, $s0, $t1 #n-i
	addi $a0, $a0, -1 #n-i-1
	jal tenpower
	mul $t6, $v0, $s3 #tenpower(n-i-1)*j
	lw $ra, 0($sp)
	add $sp, $sp, 4 #restore stack
	add $s2, $s2, $t6
	addi $s3, $s3, 1 #next iteration
	j whileStoi
	stopStoi:
    add $v0, $zero, $s2
    jr $ra
strlen:#string is stored in $a0
	addi $sp, $sp , -4
	sw $a0, 0($sp)
	add $t0, $0, $0
	whilestrlen:	lb $t1, 0($a0)
		beqz $t1, stopStrlen
		addi $t0, $t0, 1 #increase char count
		addi $a0, $a0, 1 #continue iteration
		j whilestrlen
	stopStrlen:	add $v0, $t0, $zero
	lw $a0, 0($sp)
	addi $sp, $sp, 4
	jr $ra
tenpower:
	addi $t0, $zero, 1 #result
	add $t1, $a0, $0 #parameter
	loopTenpower:	beq $t1, 0, stopTenpower
		add $t2, $0, 10
		mul $t0, $t0, $t2
		addi $t1, $t1, -1
		j loopTenpower
	stopTenpower:	add $v0, $zero, $t0
	add $t0 , $0, $0
	jr $ra
exit:
	li $v0, 10
	syscall
