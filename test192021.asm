.data
val: .word -1,-2,4,0
.text

lui $a0, 0x1001
la $a0, val
ori $v0, $0 , 0
loop:
	lb $t1, 0($a0)
	beq $t1, $0, exit
	addu $v0, $v0, $t1
	addi $a0, $a0, 1
	add $t1, $0, $0
	j loop
exit:
	li $v0, 10
	syscall
